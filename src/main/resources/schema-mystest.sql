
CREATE TABLE `mydb`.`useraccounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `creation_time` blob NOT NULL,
  `modification_time` blob NOT NULL,
  `version` bigint(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) NOT NULL,
  `sign_in_provider` varchar(20) DEFAULT NULL,
  `authorize_access` VARCHAR(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_f9sl209luxhu4rylls0h1m625` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
