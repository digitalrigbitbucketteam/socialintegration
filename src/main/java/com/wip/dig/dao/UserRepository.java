package com.wip.dig.dao;

import com.wip.dig.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by punitc on 9/24/15.
 */
public interface UserRepository extends JpaRepository<User,Long> {

    public User findByUserId(String user);

}
