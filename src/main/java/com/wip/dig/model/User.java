package com.wip.dig.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.time.DateTime;
import javax.persistence.*;

@Entity
@Table(name = "useraccounts")
public class User  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "user_id", length = 100, nullable = false, unique = true)
    private String userId;

    @Column(name = "email", length = 100, nullable = false)
    private String email;

    @Column(name = "first_name", length = 100,nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 100, nullable = false)
    private String lastName;

    @Column(name = "password", length = 255)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "role", length = 20, nullable = false)
    private Role role;

    @Enumerated(EnumType.STRING)
    @Column(name = "sign_in_provider", length = 20)
    private SocialMediaService signInProvider;

    @Column(name = "creation_time", nullable = false, length = 300)
    private DateTime creationTime;

    @Column(name = "modification_time", nullable = false, length = 300)
    private DateTime modificationTime;

    @Column (name="authorize_access", nullable = false, length = 1)
    private String authorizeAccess;


    @Version
    private long version;

    public User() {

    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public String getUserId() {return userId;}

    public String getEmail() {
        return email;
    }

    public String getFirstName() { return firstName; }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public SocialMediaService getSignInProvider() {
        return signInProvider;
    }

    public DateTime getCreationTime() {
        return creationTime;
    }

    public DateTime getModificationTime() {
        return modificationTime;
    }

    public long getVersion() { return version; }

    public String getAuthorizeAccess() {
        return authorizeAccess;
    }

    // Generating setter start

    public void setId(Long id) {
        this.id = id;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setSignInProvider(SocialMediaService signInProvider) {
        this.signInProvider = signInProvider;
    }

    public void setCreationTime(DateTime creationTime) {
        this.creationTime = creationTime;
    }

    public void setModificationTime(DateTime modificationTime) {
        this.modificationTime = modificationTime;
    }

    public void setAuthorizeAccess(String authorizeAccess) {
        this.authorizeAccess = authorizeAccess;
    }

    // Generating getter start


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("creationTime", this.getCreationTime())
                .append("userId",userId)
                .append("email", email)
                .append("firstName", firstName)
                .append("lastName", lastName)
                .append("modificationTime", this.getModificationTime())
                .append("signInProvider", this.getSignInProvider())
                .append("version", this.getVersion())
                .append("authorizeAccess",this.authorizeAccess)
                .toString();
    }

    public static class Builder {

        private User user;

        public Builder() {
            user = new User();
            user.role = Role.ROLE_USER;
            user.authorizeAccess = "Y";
        }

        public Builder userId(String userId)
        {
            user.userId = userId;
            return this;
        }

        public Builder email(String email) {
            user.email = email;
            return this;
        }

        public Builder firstName(String firstName) {
            if(firstName != null)
                user.firstName = firstName;
            else
                user.firstName = "";
            return this;
        }

        public Builder lastName(String lastName) {
            if(lastName != null)
                user.lastName = lastName;
            else
                user.lastName = "";
            return this;
        }

        public Builder password(String password) {
            user.password = password;
            return this;
        }

        public Builder role(Role role){
            user.role = role;
            return this;
        }

        public Builder signInProvider(SocialMediaService signInProvider) {
            user.signInProvider = signInProvider;
            return this;
        }

        public Builder authorizeAccess(String authroizeAccess) {
            if(authroizeAccess != null)
                user.authorizeAccess = authroizeAccess;
            else
                user.authorizeAccess = "N";
            return this;
        }


        public User build() {
            return user;
        }
    }

    @PrePersist
    public void prePersist() {
        DateTime now = DateTime.now();
        this.creationTime = now;
        this.modificationTime = now;
    }

    @PreUpdate
    public void preUpdate() {
        this.modificationTime = DateTime.now();
    }
}
