package com.wip.dig.config;

import com.wip.dig.dao.UserRepository;
import com.wip.dig.model.Role;
import com.wip.dig.model.SocialMediaService;
import com.wip.dig.model.User;
import com.wip.dig.services.RepositoryUserDetailsService;
import com.wip.dig.services.SimpleSocialUsersDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.social.security.SocialUserDetailsService;
import org.springframework.social.security.SpringSocialConfigurer;

import javax.sql.DataSource;

/**
 * Created by punitc on 9/9/15.
 */

@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityConfig.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DataSource dataSource;

    User loadUser = new User();

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private Environment env;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        LOGGER.debug("configureGlobal - userRepository", userRepository);

//      Use the next command for settting inmemory authentication
//        auth
//                .inMemoryAuthentication()
//                .withUser("user").password("password").roles("USER");

        loadDefaultUser();

        auth
                .userDetailsService(userDetailsService())
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        LOGGER.debug("configure http");
        http
                .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/login/authenticate")
                    .failureUrl("/login?param.error=bad_credentials")
                    .permitAll()
                .and()
                    .logout()
                    .logoutUrl("/logout")
                    .deleteCookies("JSESSIONID")
                .and()
                    .authorizeRequests()
                    .antMatchers("/static/**").permitAll()
                    .antMatchers("/useradmin").hasRole("ADMIN")
                    .antMatchers("/**").authenticated()
                .and()
                    .rememberMe()
                .and()
                    .apply(new SpringSocialConfigurer()
                            .postLoginUrl("/")
                            .alwaysUsePostLoginUrl(true))
                .and()
                .csrf().disable();

        http.sessionManagement().maximumSessions(1).sessionRegistry(sessionRegistry());
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new RepositoryUserDetailsService(userRepository);
    }

    @Bean
    public SocialUserDetailsService socialUsersDetailService() {
        return new SimpleSocialUsersDetailService(userDetailsService());
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
        return new ServletListenerRegistrationBean<HttpSessionEventPublisher>(new HttpSessionEventPublisher());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(10);
    }

    public void loadDefaultUser()
    {
        LOGGER.debug("Loading default user");

        User.Builder usr1 = loadUser.getBuilder();
        usr1.userId(env.getProperty("com.wip.dig.default.user1.userID"))
                .firstName(env.getProperty("com.wip.dig.default.user1.firstName"))
                .lastName(env.getProperty("com.wip.dig.default.user1.lastName"))
                .email(env.getProperty("com.wip.dig.default.user1.email"))
                .password(env.getProperty("com.wip.dig.default.user1.password"))
                .role(Role.valueOf(env.getProperty("com.wip.dig.default.user1.role")))
                .authorizeAccess("A")
                .signInProvider(SocialMediaService.form);
        loadUser = usr1.build();
        userRepository.save(loadUser);
        LOGGER.debug("Loaded user - Admin role "+loadUser);

        User.Builder usr2 = loadUser.getBuilder();
        usr2.userId(env.getProperty("com.wip.dig.default.user2.userID"))
                .firstName(env.getProperty("com.wip.dig.default.user2.firstName"))
                .lastName(env.getProperty("com.wip.dig.default.user2.lastName"))
                .email(env.getProperty("com.wip.dig.default.user2.email"))
                .password(env.getProperty("com.wip.dig.default.user2.password"))
                .role(Role.valueOf(env.getProperty("com.wip.dig.default.user2.role")))
                .authorizeAccess("Y")
                .signInProvider(SocialMediaService.form);
        loadUser = usr2.build();
        userRepository.save(loadUser);
        LOGGER.debug("Loaded user - User role "+loadUser);
    }
}
