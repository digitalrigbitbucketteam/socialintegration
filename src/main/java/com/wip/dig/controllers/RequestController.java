package com.wip.dig.controllers;

import com.wip.dig.dao.UserRepository;
import com.wip.dig.model.AuthrisedUserDetail;
import com.wip.dig.model.Role;
import com.wip.dig.model.SocialMediaService;
import com.wip.dig.model.User;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PostData;
import org.springframework.social.linkedin.api.LinkedIn;
import org.springframework.social.security.SocialAuthenticationToken;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

/**
 * Created by punitc on 9/9/15.
 */

@Controller
public class RequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestController.class);

    @Autowired
    private DataSource dataSource;

    @Autowired
    private final Facebook facebook;

    @Autowired
    private Twitter twitter;

    @Autowired
    private LinkedIn linkedIn;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRegistry sessionRegistry;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Autowired
    public RequestController(Facebook facebook) {
        this.facebook = facebook;
    }

    /**
     *
     * @param request
     * @param currentUser
     * @param model
     * @return
     */
    @RequestMapping("/home")
    public String home(HttpServletRequest request, Principal currentUser, Model model) {
        LOGGER.debug("Request /home - user " + currentUser);
        User userlocal = userRepository.findByUserId(currentUser.getName());
        LOGGER.debug("DB user detail " + userlocal);
        model.addAttribute("currentUserConnection", userlocal);
        return "home";
    }

    /**
     *
     * @param request
     * @param currentUser
     * @param model
     * @return
     */
    @RequestMapping("/postmessage")
    public String postmessage(HttpServletRequest request, Principal currentUser, Model model) {

        LOGGER.debug("Request /postmessage - user " + currentUser);
        LOGGER.debug("Posting message " + request.getParameter("comment"));

        if (currentUser instanceof SocialAuthenticationToken) {
            if (((SocialAuthenticationToken) currentUser).getProviderId().toString().equals(SocialMediaService.facebook.name())) {
                PostData postData = new PostData(facebook.userOperations().getUserProfile().getId());
                postData.message(DateTime.now() + " Message -- " + request.getParameter("comment"));
                facebook.feedOperations().post(postData);
            }

            if (((SocialAuthenticationToken) currentUser).getProviderId().toString().equals(SocialMediaService.twitter.name())) {
                System.out.println("Posting twitter message");
                String profileid = twitter.userOperations().getScreenName();
                twitter.timelineOperations().updateStatus(DateTime.now() + " Message -- " + request.getParameter("comment"));
            }

            if (((SocialAuthenticationToken) currentUser).getProviderId().toString().equals(SocialMediaService.linkedin.name())) {
                // Any logic for linked in usage
            }

        }
        return "redirect:/home";
    }


    /**
     *
     * @param request
     * @param currentUser
     * @param model
     * @return
     */
    @RequestMapping("/login")
    public String login(HttpServletRequest request, Principal currentUser, Model model) {
        LOGGER.debug("Request /login - user " + currentUser);

        //TODO check refer value for setting back
        String referrer = request.getHeader("referer");
        if (referrer != null) {
            request.getSession().setAttribute("url_prior_login", referrer);
        }
        return "login";
    }

    /**
     *
     * @param request
     * @param currentUser
     * @param model
     * @param response
     * @return
     */
    @RequestMapping("/")
    public String slash(HttpServletRequest request, Principal currentUser, Model model, HttpServletResponse response) {
        LOGGER.debug("Request / (root) " + currentUser);
        // TODO Add any preprocessing logic required for the user
        return "redirect:/home";
    }

    /**
     *
     * @param request
     * @param currentUser
     * @param model
     * @param response
     * @return
     */
    @RequestMapping("/useradmin")
    public String useradmin(HttpServletRequest request, Principal currentUser, Model model, HttpServletResponse response) {
        LOGGER.debug("Request /useradmin " + currentUser);

        List<User> luser = userRepository.findAll();
        model.addAllAttributes(luser);
        model.addAttribute("users", luser);

        User userlocal = userRepository.findByUserId(currentUser.getName());
        model.addAttribute("currentUserConnection", userlocal);

        LOGGER.debug("Sending user info {}", luser);
        return "administration";
    }

    /**
     *
     * @param request
     * @param currentUser
     * @param model
     * @return
     */
    @RequestMapping("/adduser")
    public String adduser(HttpServletRequest request, Principal currentUser, Model model) {
        LOGGER.debug("Request /adduser");
        User userlocal = userRepository.findByUserId(currentUser.getName());
        model.addAttribute("currentUserConnection", userlocal);
        return "adduser";
    }


    /**
     *
     * @param request
     * @param currentUser
     * @param model
     * @param response
     * @param user
     * @return
     */
    @RequestMapping("/adduserprocess")
    public String adduserprocess(HttpServletRequest request, Principal currentUser, Model model, HttpServletResponse response, @ModelAttribute(value = "foo") User user) {
        LOGGER.debug("Request /adduserprocess " + currentUser);
        LOGGER.debug("Process user object " + user);

        User dbuser = userRepository.findByUserId(user.getUserId());
        if (dbuser == null) {
            user.setCreationTime(DateTime.now());
            user.setModificationTime(DateTime.now());
            user.setSignInProvider(SocialMediaService.form);
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setRole(Role.ROLE_USER);
            user.setAuthorizeAccess("Y");
            userRepository.save(user);
        } else if (dbuser.getSignInProvider().equals(SocialMediaService.form)) {
            dbuser.setFirstName(user.getFirstName());
            dbuser.setLastName(user.getLastName());
            dbuser.setEmail(user.getEmail());
            dbuser.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(dbuser);
        }

        return "redirect:/useradmin";
    }


    /**
     *
     * @param request
     * @param currentUser
     * @param model
     * @param response
     * @param user
     * @return
     */
    @RequestMapping(value = "/processRequest", method = RequestMethod.POST)
    public String processrequest(HttpServletRequest request, Principal currentUser, Model model, HttpServletResponse response, @ModelAttribute(value = "user") User user) {

        LOGGER.debug("Request /processRequest " + currentUser);
        LOGGER.debug("Processing user " + user);

        User userlocal = userRepository.findByUserId(user.getUserId());
        userlocal.setAuthorizeAccess(user.getAuthorizeAccess());
        userlocal.setModificationTime(DateTime.now());

        userRepository.save(userlocal);

        List<Object> loggedUsers = sessionRegistry.getAllPrincipals();

        System.out.println("Logged user " + loggedUsers);

        for (Object principal : loggedUsers) {

            System.out.println("Logged in user instance " + principal.getClass());
            if (principal instanceof AuthrisedUserDetail) {
                final AuthrisedUserDetail loggedUser = (AuthrisedUserDetail) principal;
                if (user.getUserId().equals(loggedUser.getUserId())) {
                    List<SessionInformation> sessionsInfo = sessionRegistry.getAllSessions(principal, false);
                    if (null != sessionsInfo && sessionsInfo.size() > 0) {
                        for (SessionInformation sessionInformation : sessionsInfo) {
                            System.out.println("Exprire now :" + sessionInformation.getSessionId());
                            sessionInformation.expireNow();
                            sessionRegistry.removeSessionInformation(sessionInformation.getSessionId());
                            SecurityContextHolder.clearContext();
                            // User is not forced to re-logging
                        }
                    }
                }
            }
        }

        return "redirect:/useradmin";
    }


}
