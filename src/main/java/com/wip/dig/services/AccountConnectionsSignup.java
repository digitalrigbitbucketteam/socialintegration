package com.wip.dig.services;


import com.wip.dig.dao.UserRepository;
import com.wip.dig.model.SocialMediaService;
import com.wip.dig.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;

import java.util.UUID;

/**
 * Created by punitc on 9/15/15.
 */

public class AccountConnectionsSignup implements ConnectionSignUp {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountConnectionsSignup.class);

    private User userNew;

    private UserRepository userRepository;

    public AccountConnectionsSignup(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String execute(Connection<?> connection) {
        LOGGER.debug("Entering execute - Account Signup");
        LOGGER.debug("key " + connection.getKey() + " Profile url " + connection.getProfileUrl());
        org.springframework.social.connect.UserProfile profile = connection.fetchUserProfile();
        String userId = UUID.randomUUID().toString();
        // TODO: Or simply use: r = new Random(); r.nextInt(); ???

        LOGGER.debug("Created user-id: " + userId);
        User.Builder blr = null;

        if (profile.getEmail() == null)
        {
            blr = userNew.getBuilder().firstName(profile.getFirstName()).lastName(profile.getLastName()).email(profile.getUsername()).signInProvider(SocialMediaService.valueOf(connection.getKey().getProviderId())).userId(userId);
        }
        else {
            blr = userNew.getBuilder().firstName(profile.getFirstName()).lastName(profile.getLastName()).email(profile.getEmail()).signInProvider(SocialMediaService.valueOf(connection.getKey().getProviderId())).userId(userId);
        }
        userNew = blr.build();

        LOGGER.debug("Creating user -- Name "+userNew);
        userRepository.save(userNew);
        return userId;
    }
}