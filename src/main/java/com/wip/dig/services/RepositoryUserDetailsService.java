package com.wip.dig.services;

import com.wip.dig.dao.UserRepository;
import com.wip.dig.model.AuthrisedUserDetail;
import com.wip.dig.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by punitc on 9/10/15.
 */
public class RepositoryUserDetailsService implements UserDetailsService {


    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryUserDetailsService.class);

    private UserRepository repository;

    @Autowired
    Environment env;

    @Autowired
    public RepositoryUserDetailsService(UserRepository repository) {
        this.repository = repository;
    }

    /**
     * Loads the user information.
     *
     * @param username The username of the requested user.
     * @return The information of the user.
     * @throws UsernameNotFoundException Thrown if no user is found with the given username.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.debug("Loading user by username: {}", username);

        User user = repository.findByUserId(username);

        LOGGER.debug("Found user in useraccount : {}", user);

        if (user == null) {
            throw new UsernameNotFoundException("No user found with username: " + username);
        }


        if (checkAuthorize()) {
            LOGGER.debug("****** Authorise check enabled *********");
            if (user.getAuthorizeAccess().equals("N"))
                throw new UsernameNotFoundException("User not authorized" + username);
        }

        AuthrisedUserDetail principal = AuthrisedUserDetail.getBuilder()
                .firstName(user.getFirstName())
                .id(user.getId())
                .lastName(user.getLastName())
                .password(user.getPassword())
                .role(user.getRole())
                .socialSignInProvider(user.getSignInProvider())
                .username(user.getUserId())
                .build();

        LOGGER.debug("Principle object returing"+ principal);
        return principal;
    }


    private boolean checkAuthorize()
    {
        String authroizeValue = env.getProperty("com.wip.dig.authroisationcheck");
        Boolean returnValue = false;
        if(authroizeValue != null)
            returnValue = Boolean.valueOf(authroizeValue);
        return returnValue;
    }
}

