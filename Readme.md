# Spring security - Social & Form based Authentication #

```
#!
This code base contains complete project i.e including intellij files, so if you have intellij if your system pull out to complete project base and you are good to run. For team planning to use just the code base into there existing project do following steps
1. Copy content of build.gradle into your project
2. Copy content under src folder
3. Run $gradle idea (at terminal to pull all project dependency)

To test the project
1. start the application with command $gradle clean build run
2. Open browser with url http://localhost:8080/home
3. Login with two default user created
	1. User1/password - Role Admin
	2. User2/password - Role User 

Social login: This code base contains clientId & secretkey for Facebook, LinkedIn & Twitter using default application. To use this for your specific usage update these keys at application.properties file

Managing user: You can restrict user access to application at http://localhost:8080/useradmin - Login with Admin user (User1) to authorize, de-authorize social user. This also allows you to add/modify system user

```